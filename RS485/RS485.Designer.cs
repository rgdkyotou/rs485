﻿namespace RS485
{
    partial class RS485
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Output = new System.Windows.Forms.TextBox();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.File = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Start = new System.Windows.Forms.CheckBox();
            this.InputText = new System.Windows.Forms.TextBox();
            this.SEND = new System.Windows.Forms.Button();
            this.REX_Receive = new System.Windows.Forms.TextBox();
            this.servo_ID = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.SEND_ACK = new System.Windows.Forms.Button();
            this.SEND_DEV = new System.Windows.Forms.ComboBox();
            this.BAUD = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SEND_RESET = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SEND_ON = new System.Windows.Forms.Button();
            this.GET_ANGLE = new System.Windows.Forms.Button();
            this.QUERY = new System.Windows.Forms.Button();
            this.WRITE_ID = new System.Windows.Forms.Button();
            this.SEND_MOVE = new System.Windows.Forms.Button();
            this.move_time = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.move_angle = new System.Windows.Forms.NumericUpDown();
            this.RSC_Receive = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.new_ID = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.L_limit = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.R_limit = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.T_limit = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.new_BAUD = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.Reply = new System.Windows.Forms.NumericUpDown();
            this.mainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.servo_ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.move_time)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.move_angle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.new_ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.L_limit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.R_limit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_limit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Reply)).BeginInit();
            this.SuspendLayout();
            // 
            // Output
            // 
            this.Output.HideSelection = false;
            this.Output.Location = new System.Drawing.Point(14, 229);
            this.Output.Multiline = true;
            this.Output.Name = "Output";
            this.Output.ReadOnly = true;
            this.Output.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Output.Size = new System.Drawing.Size(624, 122);
            this.Output.TabIndex = 0;
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.File});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(624, 24);
            this.mainMenu.TabIndex = 1;
            this.mainMenu.Text = "mainMenu";
            // 
            // File
            // 
            this.File.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeToolStripMenuItem});
            this.File.Name = "File";
            this.File.Size = new System.Drawing.Size(39, 20);
            this.File.Text = "File";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // Start
            // 
            this.Start.Appearance = System.Windows.Forms.Appearance.Button;
            this.Start.AutoSize = true;
            this.Start.Location = new System.Drawing.Point(158, 29);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(40, 22);
            this.Start.TabIndex = 0;
            this.Start.Text = "Start";
            this.Start.UseVisualStyleBackColor = true;
            this.Start.CheckedChanged += new System.EventHandler(this.Start_CheckedChanged);
            // 
            // InputText
            // 
            this.InputText.Location = new System.Drawing.Point(308, 59);
            this.InputText.Name = "InputText";
            this.InputText.Size = new System.Drawing.Size(223, 19);
            this.InputText.TabIndex = 0;
            this.toolTip1.SetToolTip(this.InputText, "テストのため，任意の文字列を送信できます");
            // 
            // SEND
            // 
            this.SEND.Enabled = false;
            this.SEND.Location = new System.Drawing.Point(537, 55);
            this.SEND.Name = "SEND";
            this.SEND.Size = new System.Drawing.Size(75, 23);
            this.SEND.TabIndex = 1;
            this.SEND.Text = "SEND TEXT";
            this.SEND.UseVisualStyleBackColor = true;
            this.SEND.Click += new System.EventHandler(this.send_Click);
            // 
            // REX_Receive
            // 
            this.REX_Receive.Location = new System.Drawing.Point(0, 488);
            this.REX_Receive.Multiline = true;
            this.REX_Receive.Name = "REX_Receive";
            this.REX_Receive.ReadOnly = true;
            this.REX_Receive.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.REX_Receive.Size = new System.Drawing.Size(624, 68);
            this.REX_Receive.TabIndex = 0;
            // 
            // servo_ID
            // 
            this.servo_ID.Location = new System.Drawing.Point(34, 84);
            this.servo_ID.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.servo_ID.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.servo_ID.Name = "servo_ID";
            this.servo_ID.Size = new System.Drawing.Size(44, 19);
            this.servo_ID.TabIndex = 3;
            this.servo_ID.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "ID";
            // 
            // SEND_ACK
            // 
            this.SEND_ACK.Enabled = false;
            this.SEND_ACK.Location = new System.Drawing.Point(88, 81);
            this.SEND_ACK.Name = "SEND_ACK";
            this.SEND_ACK.Size = new System.Drawing.Size(75, 23);
            this.SEND_ACK.TabIndex = 5;
            this.SEND_ACK.Text = "ACK";
            this.toolTip1.SetToolTip(this.SEND_ACK, "モーターを点呼します");
            this.SEND_ACK.UseVisualStyleBackColor = true;
            this.SEND_ACK.Click += new System.EventHandler(this.send_ACK_Click);
            // 
            // SEND_DEV
            // 
            this.SEND_DEV.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SEND_DEV.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.SEND_DEV.FormattingEnabled = true;
            this.SEND_DEV.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.SEND_DEV.Location = new System.Drawing.Point(8, 57);
            this.SEND_DEV.Name = "SEND_DEV";
            this.SEND_DEV.Size = new System.Drawing.Size(294, 20);
            this.SEND_DEV.TabIndex = 8;
            this.toolTip1.SetToolTip(this.SEND_DEV, "送信に使うデバイス");
            // 
            // BAUD
            // 
            this.BAUD.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.BAUD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.BAUD.FormattingEnabled = true;
            this.BAUD.Items.AddRange(new object[] {
            "9600",
            "14400",
            "19200",
            "28800",
            "38400",
            "57600",
            "76800",
            "115200",
            "153600",
            "230400",
            "460800"});
            this.BAUD.Location = new System.Drawing.Point(53, 31);
            this.BAUD.Name = "BAUD";
            this.BAUD.Size = new System.Drawing.Size(85, 20);
            this.BAUD.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 12);
            this.label2.TabIndex = 10;
            this.label2.Text = "BAUD";
            // 
            // SEND_RESET
            // 
            this.SEND_RESET.Enabled = false;
            this.SEND_RESET.Location = new System.Drawing.Point(537, 192);
            this.SEND_RESET.Name = "SEND_RESET";
            this.SEND_RESET.Size = new System.Drawing.Size(75, 23);
            this.SEND_RESET.TabIndex = 11;
            this.SEND_RESET.Text = "FORMAT";
            this.toolTip1.SetToolTip(this.SEND_RESET, "接続されているモーターを全部リセットします．\r\nIDは全部１になるので，複数台のモーターを\r\n接続すると誤動作します．");
            this.SEND_RESET.UseVisualStyleBackColor = true;
            this.SEND_RESET.Click += new System.EventHandler(this.SEND_RESET_Click);
            // 
            // SEND_ON
            // 
            this.SEND_ON.Enabled = false;
            this.SEND_ON.Location = new System.Drawing.Point(169, 81);
            this.SEND_ON.Name = "SEND_ON";
            this.SEND_ON.Size = new System.Drawing.Size(75, 23);
            this.SEND_ON.TabIndex = 13;
            this.SEND_ON.Text = "ON";
            this.toolTip1.SetToolTip(this.SEND_ON, "トルクのON/OFFを設定します");
            this.SEND_ON.UseVisualStyleBackColor = true;
            this.SEND_ON.Click += new System.EventHandler(this.SEND_ON_Click);
            // 
            // GET_ANGLE
            // 
            this.GET_ANGLE.Enabled = false;
            this.GET_ANGLE.Location = new System.Drawing.Point(89, 138);
            this.GET_ANGLE.Name = "GET_ANGLE";
            this.GET_ANGLE.Size = new System.Drawing.Size(75, 23);
            this.GET_ANGLE.TabIndex = 19;
            this.GET_ANGLE.Text = "STATUS";
            this.toolTip1.SetToolTip(this.GET_ANGLE, "運行状況を取得します");
            this.GET_ANGLE.UseVisualStyleBackColor = true;
            this.GET_ANGLE.Click += new System.EventHandler(this.GET_ANGLE_Click);
            // 
            // QUERY
            // 
            this.QUERY.Enabled = false;
            this.QUERY.Location = new System.Drawing.Point(8, 138);
            this.QUERY.Name = "QUERY";
            this.QUERY.Size = new System.Drawing.Size(75, 23);
            this.QUERY.TabIndex = 21;
            this.QUERY.Text = "INFO";
            this.toolTip1.SetToolTip(this.QUERY, "設定値を取得します");
            this.QUERY.UseVisualStyleBackColor = true;
            this.QUERY.Click += new System.EventHandler(this.QUERY_Click);
            // 
            // WRITE_ID
            // 
            this.WRITE_ID.Enabled = false;
            this.WRITE_ID.Location = new System.Drawing.Point(456, 192);
            this.WRITE_ID.Name = "WRITE_ID";
            this.WRITE_ID.Size = new System.Drawing.Size(75, 23);
            this.WRITE_ID.TabIndex = 24;
            this.WRITE_ID.Text = "WRITE";
            this.toolTip1.SetToolTip(this.WRITE_ID, "変更したパラメータをEEPROMに書き込みます．");
            this.WRITE_ID.UseVisualStyleBackColor = true;
            this.WRITE_ID.Click += new System.EventHandler(this.WRITE_ID_Click);
            // 
            // SEND_MOVE
            // 
            this.SEND_MOVE.Enabled = false;
            this.SEND_MOVE.Location = new System.Drawing.Point(333, 109);
            this.SEND_MOVE.Name = "SEND_MOVE";
            this.SEND_MOVE.Size = new System.Drawing.Size(65, 23);
            this.SEND_MOVE.TabIndex = 12;
            this.SEND_MOVE.Text = "MOVE";
            this.SEND_MOVE.UseVisualStyleBackColor = true;
            this.SEND_MOVE.Click += new System.EventHandler(this.SEND_MOVE_Click);
            // 
            // move_time
            // 
            this.move_time.Location = new System.Drawing.Point(88, 112);
            this.move_time.Maximum = new decimal(new int[] {
            8000,
            0,
            0,
            0});
            this.move_time.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.move_time.Name = "move_time";
            this.move_time.Size = new System.Drawing.Size(93, 19);
            this.move_time.TabIndex = 15;
            this.move_time.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 12);
            this.label3.TabIndex = 16;
            this.label3.Text = "speed(10ms)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(186, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 12);
            this.label4.TabIndex = 17;
            this.label4.Text = "angle(deg)";
            // 
            // move_angle
            // 
            this.move_angle.Location = new System.Drawing.Point(248, 112);
            this.move_angle.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.move_angle.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
            this.move_angle.Name = "move_angle";
            this.move_angle.Size = new System.Drawing.Size(80, 19);
            this.move_angle.TabIndex = 18;
            this.move_angle.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // RSC_Receive
            // 
            this.RSC_Receive.Location = new System.Drawing.Point(0, 357);
            this.RSC_Receive.Multiline = true;
            this.RSC_Receive.Name = "RSC_Receive";
            this.RSC_Receive.ReadOnly = true;
            this.RSC_Receive.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.RSC_Receive.Size = new System.Drawing.Size(624, 125);
            this.RSC_Receive.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 170);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 12);
            this.label5.TabIndex = 22;
            this.label5.Text = "new-ID";
            // 
            // new_ID
            // 
            this.new_ID.Enabled = false;
            this.new_ID.Location = new System.Drawing.Point(60, 167);
            this.new_ID.Maximum = new decimal(new int[] {
            127,
            0,
            0,
            0});
            this.new_ID.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.new_ID.Name = "new_ID";
            this.new_ID.Size = new System.Drawing.Size(44, 19);
            this.new_ID.TabIndex = 23;
            this.new_ID.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.new_ID.ValueChanged += new System.EventHandler(this.new_ID_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 194);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 12);
            this.label6.TabIndex = 25;
            this.label6.Text = "L-limit";
            // 
            // L_limit
            // 
            this.L_limit.Enabled = false;
            this.L_limit.Location = new System.Drawing.Point(60, 192);
            this.L_limit.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.L_limit.Minimum = new decimal(new int[] {
            150,
            0,
            0,
            -2147483648});
            this.L_limit.Name = "L_limit";
            this.L_limit.Size = new System.Drawing.Size(80, 19);
            this.L_limit.TabIndex = 26;
            this.L_limit.ValueChanged += new System.EventHandler(this.L_limit_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(146, 194);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 27;
            this.label7.Text = "R-limit";
            // 
            // R_limit
            // 
            this.R_limit.Enabled = false;
            this.R_limit.Location = new System.Drawing.Point(193, 192);
            this.R_limit.Maximum = new decimal(new int[] {
            150,
            0,
            0,
            0});
            this.R_limit.Minimum = new decimal(new int[] {
            150,
            0,
            0,
            -2147483648});
            this.R_limit.Name = "R_limit";
            this.R_limit.Size = new System.Drawing.Size(80, 19);
            this.R_limit.TabIndex = 28;
            this.R_limit.ValueChanged += new System.EventHandler(this.R_limit_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(288, 194);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 12);
            this.label8.TabIndex = 29;
            this.label8.Text = "T-limit";
            // 
            // T_limit
            // 
            this.T_limit.Enabled = false;
            this.T_limit.Location = new System.Drawing.Point(329, 192);
            this.T_limit.Maximum = new decimal(new int[] {
            105,
            0,
            0,
            0});
            this.T_limit.Name = "T_limit";
            this.T_limit.ReadOnly = true;
            this.T_limit.Size = new System.Drawing.Size(80, 19);
            this.T_limit.TabIndex = 30;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(126, 170);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 12);
            this.label9.TabIndex = 31;
            this.label9.Text = "new_BAUD";
            // 
            // new_BAUD
            // 
            this.new_BAUD.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.new_BAUD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.new_BAUD.Enabled = false;
            this.new_BAUD.FormattingEnabled = true;
            this.new_BAUD.Items.AddRange(new object[] {
            "9600",
            "14400",
            "19200",
            "28800",
            "38400",
            "57600",
            "76800",
            "115200",
            "153600",
            "230400",
            "460800"});
            this.new_BAUD.Location = new System.Drawing.Point(193, 167);
            this.new_BAUD.Name = "new_BAUD";
            this.new_BAUD.Size = new System.Drawing.Size(85, 20);
            this.new_BAUD.TabIndex = 32;
            this.new_BAUD.SelectedIndexChanged += new System.EventHandler(this.new_BAUD_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(288, 169);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 12);
            this.label10.TabIndex = 33;
            this.label10.Text = "Reply in (us)";
            // 
            // Reply
            // 
            this.Reply.Enabled = false;
            this.Reply.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.Reply.Location = new System.Drawing.Point(365, 167);
            this.Reply.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.Reply.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.Reply.Name = "Reply";
            this.Reply.Size = new System.Drawing.Size(80, 19);
            this.Reply.TabIndex = 34;
            this.Reply.Value = new decimal(new int[] {
            350,
            0,
            0,
            0});
            this.Reply.ValueChanged += new System.EventHandler(this.Reply_ValueChanged);
            // 
            // RS485
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 554);
            this.Controls.Add(this.Reply);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.new_BAUD);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.T_limit);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.R_limit);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.L_limit);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.WRITE_ID);
            this.Controls.Add(this.new_ID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.QUERY);
            this.Controls.Add(this.RSC_Receive);
            this.Controls.Add(this.GET_ANGLE);
            this.Controls.Add(this.move_angle);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.move_time);
            this.Controls.Add(this.SEND_ON);
            this.Controls.Add(this.SEND_MOVE);
            this.Controls.Add(this.SEND_RESET);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BAUD);
            this.Controls.Add(this.SEND_DEV);
            this.Controls.Add(this.SEND_ACK);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.servo_ID);
            this.Controls.Add(this.Start);
            this.Controls.Add(this.REX_Receive);
            this.Controls.Add(this.Output);
            this.Controls.Add(this.mainMenu);
            this.Controls.Add(this.SEND);
            this.Controls.Add(this.InputText);
            this.MainMenuStrip = this.mainMenu;
            this.Name = "RS485";
            this.Text = "RS485 Communication Test";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RS485_FormClosing);
            this.Load += new System.EventHandler(this.RS485_Load);
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.servo_ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.move_time)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.move_angle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.new_ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.L_limit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.R_limit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T_limit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Reply)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Output;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem File;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.CheckBox Start;
        private System.Windows.Forms.Button SEND;
        private System.Windows.Forms.TextBox InputText;
        private System.Windows.Forms.TextBox REX_Receive;
        private System.Windows.Forms.NumericUpDown servo_ID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button SEND_ACK;
        private System.Windows.Forms.ComboBox SEND_DEV;
        private System.Windows.Forms.ComboBox BAUD;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button SEND_RESET;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button SEND_MOVE;
        private System.Windows.Forms.Button SEND_ON;
        private System.Windows.Forms.NumericUpDown move_time;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown move_angle;
        private System.Windows.Forms.Button GET_ANGLE;
        private System.Windows.Forms.TextBox RSC_Receive;
        private System.Windows.Forms.Button QUERY;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown new_ID;
        private System.Windows.Forms.Button WRITE_ID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown L_limit;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown R_limit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown T_limit;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox new_BAUD;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown Reply;
    }
}

