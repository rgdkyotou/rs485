﻿//  モーター回すぞ
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RS485
{
    public partial class RS485 : Form
    {
        Dictionary<string, int> Devices;
        static System.IO.Ports.SerialPort REX;
        static System.IO.Ports.SerialPort FUTABA;
        public RS485()
        {
            InitializeComponent();
        }
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void rexWrite(string message)
        {
            REX_Receive.Focus();
            REX_Receive.AppendText(message);
        }
        private void rscWrite(string message)
        {
            RSC_Receive.Focus();
            RSC_Receive.AppendText(message);
        }
        private void logWrite(string message)
        {
            Output.Focus();
            Output.AppendText(message);
        }
        private void logWriteln(string message)
        {
            Output.Focus();
            Output.AppendText(message + Environment.NewLine);
        }
        private void RS485_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Start.Checked)
                Exit_Comm(Start);
        }
        private void RS485_Load(object sender, EventArgs e)
        {
            Devices = new Dictionary<string, int>();
            //  有効なPnP-COMデバイス
            System.Management.ManagementClass devices = new System.Management.ManagementClass("Win32_PnPEntity");
            foreach (System.Management.ManagementObject port in devices.GetInstances())
            {
                var _capt = port.GetPropertyValue("Caption");
                if (_capt == null) continue;
                string _name = (string)_capt;
                int _i = _name.IndexOf("(COM");
                if (_i <= 0) continue;
                int _com = Int32.Parse(_name.Substring(_i + 4).Replace(")", ""));
                _name = _name.Substring(0, _i - 1);
                //  COM_comが_nameデバイスである
                Devices[_name] = _com;
            }
            //  一覧を出力
            SEND_DEV.Items.Clear();
            foreach (string key in Devices.Keys)
            {
                SEND_DEV.Items.Add(key);
                logWriteln("Found COM" + Devices[key] + " = " + key);
            }
            SEND_DEV.SelectedIndex = SEND_DEV.Items.Count - 1;
            //  シリアルデバイス．
            REX = new System.IO.Ports.SerialPort();
            FUTABA = new System.IO.Ports.SerialPort();
            //
            BAUD.SelectedIndex = 7;
            logWriteln("準備完了っすね");
        }
        private void Start_REX(string filename)
        {
            int baud = int.Parse(BAUD.SelectedItem.ToString());
            logWriteln("Open " + filename + " [" + baud + " bps]");
            //  REX通信開始
            REX.PortName = filename;
            REX.BaudRate = baud;
            REX.Parity = System.IO.Ports.Parity.None;
            REX.DataBits = 8;
            REX.StopBits = System.IO.Ports.StopBits.One;
            REX.Handshake = System.IO.Ports.Handshake.None;    //  DTR, RTSなし
               //REXの場合，
               //  送信要求    受信可能
               //  RtsEnable   DtrEnable   送信  受信
               //  true        true        ○    △      半二重接続では受信不能
               //  true        false       ○    ×
               //  false       true        ×    ○
               //  false       false       ×    ×
               //そもそも, RS232CならRTS/DTRの導線があるので意味があるのだが，RS485では，
               //線が二本しかない．このRtsEnable, DtrEnableは，PC内部のUARTチップからRS485ドライバまでの
               //通信を制御しているだけだ．
               //で，REX-USB70旧型の制限は以下の通り：
               //  全二重通信：RTS=DTR=ON
               //  半二重SEND：RTS=ON  半二重RECV：RTS=OFF
            REX.RtsEnable = false;   //  trueにすると送信可能になる. false では，送信不許可
            REX.DtrEnable = true;   //  受信オッケー♪    ここいらはデバイス依存だ
                                    //  false, true で送信を別のデバイスで行う場合には受信できるんだけどな
            REX.ReadTimeout = 500;
            REX.WriteTimeout = 50;
            REX.Open();
            //REX_Read();   なんでか, RSC_Read()と2面まち，はできないみたいよ？わかんねえな
        }
        private void Start_RSC(string filename)
        {
            int baud = int.Parse(BAUD.SelectedItem.ToString());
            logWriteln("Open " + filename + " [" + baud + " bps]");
            //  FUTABA通信開始
            FUTABA.PortName = filename;
            FUTABA.BaudRate = baud;
            FUTABA.Parity = System.IO.Ports.Parity.None;
            FUTABA.DataBits = 8;
            FUTABA.StopBits = System.IO.Ports.StopBits.One;
            FUTABA.Handshake = System.IO.Ports.Handshake.None;    //  DTR, RTSなし
                   //FUTABAの場合：(RtsEnable,DsrEnalbe,送信できた) 
                   //  本来は RTS = 送信要求 DTR = 受信可能, なはずだが
                   //  (T,T,○)(T,F,○)(F,F,○)(F,T,○)であった．常に送信できるみたいだが・・・
            FUTABA.RtsEnable = false;
            FUTABA.DtrEnable = false;
            FUTABA.ReadTimeout = 500;
            FUTABA.WriteTimeout = 500;
            FUTABA.Open();
            RSC_Read();
        }
        private void Start_Comm(CheckBox me)
        {
            if (me.Text != "Start") return;
            int baud = int.Parse(BAUD.SelectedItem.ToString());
            //RS405CBの工場出荷設定は 115200 bps, Data:8bit, Stop:1Bit, Parity:None, FlowControl:None
            {
                //  REXデバイスを探します
                string filename = "COM";
                foreach (string key in Devices.Keys)
                {
                    if (key.IndexOf("USB RS-485") >= 0)
                    {
                        filename += Devices[key];
                    }
                }
                if (filename != "COM") Start_REX(filename);
            }
            {
                //  FUTABAデバイスを探します
                string filename = "COM";
                foreach (string key in Devices.Keys)
                {
                    if (key.IndexOf("Futaba") >= 0)
                    {
                        filename += Devices[key];
                    }

                }
                if (filename != "COM") Start_RSC(filename);
            }
            //  通信作業を実施
            logWriteln("通信を起動しました");
            me.Text = "Stop";
            BAUD.Enabled = false;
            SEND_ON.Enabled = GET_ANGLE.Enabled = QUERY.Enabled
                = SEND.Enabled = SEND_RESET.Enabled = SEND_ACK.Enabled
                = true;
            SEND_MOVE.Enabled = false;

        }
        private void Exit_Comm(CheckBox me)
        {
            logWriteln("通信を終了します.");
            me.Text = "Start";
            if (SEND_ON.Text == "OFF")
            {//モータのスイッチが入ったままである．停止
                SEND_ON.PerformClick();
                MessageBox.Show("モーターのスイッチを入れたままなんて，だめな子でちゅねー");
            }

            if (REX.IsOpen)
            {
                logWriteln("REX-CLOSE");
                REX.Close();
            }
            if (FUTABA.IsOpen)
            {
                logWriteln("FUTABA-CLOSE");
                FUTABA.Close();
            }
            SEND_ON.Enabled = GET_ANGLE.Enabled = QUERY.Enabled
                = SEND.Enabled = SEND_RESET.Enabled = SEND_ACK.Enabled
                = false;
            SEND_MOVE.Enabled = false;
            BAUD.Enabled = true;
        }
        private void Start_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox me = (CheckBox)sender;
            if (me.Checked) Start_Comm(me);
            else Exit_Comm(me);
        }
        private void send_Click(object sender, EventArgs e)
        {
            //送信デバイス
            string device = (string)SEND_DEV.Items[SEND_DEV.SelectedIndex];
            int idx = Devices[device];
            string filename = "COM" + idx.ToString();
            //送信データ
            string send_text = InputText.Text;
            if (device == "USB RS-485 Port")
            {// REXで送信する
                logWrite("REX> ");
                REX.Write(send_text + Environment.NewLine);
            }
            else
            {// FUTABAで送信する
                logWrite("RSC> ");
                FUTABA.Write(send_text + Environment.NewLine);
            }
            logWriteln(send_text);
            InputText.Clear();
        }
        private async void RSC_Read()
        {
            byte[] rest = new byte[0];
            while (Start.Checked)
            {
                Task<byte[]> myTask = Task<byte[]>.Factory.StartNew(() =>
                {
                    int rbyte;
                    try
                    {
                        rbyte = FUTABA.BytesToRead;
                    }
                    catch (System.InvalidOperationException e)
                    {//ポートが閉じてるとかのエラーですね．
                        byte[] errdata = new byte[1];
                        errdata[0] = 0x04;
                        return errdata;
                    }
                    byte[] buffer = new byte[rbyte];
                    int read = 0;
                    while (read < rbyte)
                    {
                        int length = FUTABA.Read(buffer, read, rbyte - read);
                        read += length;
                    }
                    return buffer;
                });
                try
                {
                    byte[] ret = Enumerable.Concat(rest, await myTask).ToArray();
                    bool repeat = ret.Length > 0;
                    while (repeat)
                    {
                        repeat = false;
                        if ((ret[0] == 0xFA) | (ret[0] == 0xFD))
                        {
                            int packet_size;
                            if (ret.Length < 8) continue;
                            int _id = ret[2];
                            uint _flg = ret[3];
                            int _adr = ret[4];
                            int _len = ret[5];
                            int _cnt = ret[6];
                            //  ショートパケットの長さは, 8+LEN バイト
                            bool long_packet = _id == 0;
                            packet_size = long_packet ? (8 + (1 + _len) * _cnt) : (8 + _len * _cnt);
                            if (ret.Length < packet_size) continue;
                            byte sum = ret[2];
                            for (int i = 3; i < packet_size - 1; i++) sum ^= ret[i];
                            if (ret[packet_size - 1] != sum) rscWrite("Checksum err.");
                            if (ret[0] == 0xFA)
                            {// 双葉モータへの送信データ
                                if (ret[1] != 0xAF) rscWrite("ERR");
                                if (long_packet) rscWrite("L(");
                                else
                                    rscWrite("S(");
                                rscWrite(packet_size + ") ");
                            }
                            else
                            {// 双葉モータからの返信
                                if (ret[1] != 0xDF) rscWrite("ERR");
                                if ((_flg & 0x80) != 0) rscWrite(":HeatError");
                                if ((_flg & 0x04) != 0) rscWrite(":WriteError");
                                if ((_flg & 0x01) != 0) rscWrite(":CommandError");
                                if ((_adr == 0x2A) && (_len == 18))
                                {// このパターンしか返ってこないんではないかな
                                    rscWrite("id: " + _id.ToString());
                                    double _angle = (double)BitConverter.ToInt16(ret, 7) / 10.0;
                                    double _elap = (double)BitConverter.ToInt16(ret, 9) / 100.0;
                                    double _vel = (double)BitConverter.ToInt16(ret, 11);
                                    double _amp = (double)BitConverter.ToInt16(ret, 13);
                                    double _tmp = (double)BitConverter.ToInt16(ret, 15);
                                    double _vol = (double)BitConverter.ToInt16(ret, 17) / 100.0;
                                    rscWrite(" theta= " + _angle.ToString() + " (deg)");
                                    rscWrite(" time= " + _elap.ToString() + " (ms)");
                                    rscWrite(" omega: " + _vel.ToString() + " (deg/sec)");
                                    rscWrite(" i= " + _amp.ToString() + " (mA)");
                                    rscWrite(" V= " + _vol.ToString() + " (V)");
                                    rscWrite(" T= " + _tmp.ToString() + " (Celcius)");
                                    rscWrite(Environment.NewLine);
                                }
                                else if ((_adr == 0x00) && (_len == 30))
                                {// このパターンしか返ってこないんではないかな
                                    rscWrite("id: " + _id.ToString());
                                    string _model = String.Format("{0:X}", BitConverter.ToUInt16(ret, 7));
                                    uint _version = ret[9];
                                    uint _id_ = ret[11];
                                    uint _baud;
                                    switch (ret[13])
                                    {
                                        case 0x00: _baud = 9600;break;
                                        case 0x01: _baud = 14400; break;
                                        case 0x02: _baud = 19200; break;
                                        case 0x03: _baud = 28800; break;
                                        case 0x04: _baud = 38400; break;
                                        case 0x05: _baud = 57600; break;
                                        case 0x06: _baud = 76800; break;
                                        case 0x07: _baud = 115200; break;
                                        case 0x08: _baud = 153600; break;
                                        case 0x09: _baud = 230400; break;
                                        default: _baud = 460800; break;
                                    }
                                    uint _reply = 100+(uint)ret[14]*50;
                                    double _limit_R = (double)BitConverter.ToInt16(ret, 15)/10.0;
                                    double _limit_L = (double)BitConverter.ToInt16(ret, 17) / 10.0;
                                    double _limit_T = (double)BitConverter.ToInt16(ret, 21);
                                    uint _dump = ret[23];
                                    rscWrite(" model= " + _model + " version=" + _version.ToString());
                                    rscWrite(Environment.NewLine);
                                    new_ID.Value = _id_; new_ID.Enabled = true; new_ID.Tag = false;
                                    new_BAUD.SelectedIndex = new_BAUD.Items.IndexOf(_baud.ToString());
                                    new_BAUD.Enabled = true; new_BAUD.Tag = false;
                                    Reply.Value = _reply; Reply.Enabled = true; Reply.Tag = false;
                                    L_limit.Value = (int)_limit_L; L_limit.Enabled = true; L_limit.Tag = false;
                                    R_limit.Value = (int)_limit_R; R_limit.Enabled = true; R_limit.Tag = false;
                                    T_limit.Value = (int)_limit_T; T_limit.Enabled = true; T_limit.Tag = false;
                                    WRITE_ID.Enabled = false;
                                }
                                else
                                {
                                    rscWrite("R<");
                                    rscWrite(string.Format("{0:X2}", ret[0]));
                                    for (int i = 1; i < packet_size; i++)
                                    {
                                        rscWrite(string.Format(" {0:X2}", ret[i]));
                                    }
                                    rscWrite("> " + Environment.NewLine);
                                }
                            }
                            //  1パケット読めたので, バッファーから削除
                            int copy_sz = (int)ret.Length - (packet_size);
                            rest = new byte[copy_sz];
                            Array.Copy(ret, packet_size, rest, 0, copy_sz);
                            ret = new byte[copy_sz];
                            rest.CopyTo(ret, 0);
                        }
                        else if (ret[0] == 0x04)
                        {//EOD
                            return;
                        }
                        else if (ret[0] == 0x07)
                        {//ACK
                            rscWrite("POSITIVE, MAJOR" + Environment.NewLine);
                            int copy_sz = (int)ret.Length - 1;
                            rest = new byte[copy_sz];
                            Array.Copy(ret, 1, rest, 0, copy_sz);
                            ret = new byte[copy_sz];
                            rest.CopyTo(ret, 0);
                        }
                        else if (ret[0] == 0x08)
                        {//NACK
                            rscWrite("NAGATIVE, MAJOR" + Environment.NewLine);
                            int copy_sz = (int)ret.Length - 1;
                            rest = new byte[copy_sz];
                            Array.Copy(ret, 1, rest, 0, copy_sz);
                            ret = new byte[copy_sz];
                            rest.CopyTo(ret, 0);
                        }
                        else
                        {
                            logWrite("TEXT<");
                            for (int i = 0; i < ret.Length; i++)
                            {
                                logWrite(string.Format("{0:X2}", ret[i]) + " ");
                            }
                            logWrite(">" + Environment.NewLine);
                            ret = new byte[0];
                            rest = new byte[0];
                        }
                        repeat = ret.Length > 0;
                    }
                }
                catch (InvalidCastException e)
                {
                    MessageBox.Show("えらーしたにゃ. しねよぼけ");
                }
                catch (TaskCanceledException e)
                {
                    MessageBox.Show("これって，いつおこるのかにゃあ");
                }
            }
        }
        private async void REX_Read()
        {
            byte[] rest = new byte[0];
            while (Start.Checked)
            {
                Task<byte[]> myTask = Task<byte[]>.Factory.StartNew(() =>
                {
                    //REX_token.Token.ThrowIfCancellationRequested();
                    int rbyte;
                    try
                    {
                        rbyte = REX.BytesToRead;
                    }
                    catch (System.InvalidOperationException e)
                    {//ポートが閉じてるとかのエラーですね．
                        byte[] errdata = new byte[1];
                        errdata[0] = 0x04;
                        return errdata;
                    }
                    byte[] buffer = new byte[rbyte];
                    int read = 0;
                    while (read < rbyte)
                    {
                        int length = REX.Read(buffer, read, rbyte - read);
                        read += length;
                    }
                    return buffer;
                });
                try
                {
                    byte[] ret = Enumerable.Concat(rest, await myTask).ToArray();
                    bool repeat = ret.Length > 0;
                    while (repeat)
                    {
                        repeat = false;
                        if ((ret[0] == 0xFA) | (ret[0] == 0xFD))
                        {
                            int packet_size;
                            if (ret.Length < 8) continue;
                            int _id = ret[2];
                            int _flg = ret[3];
                            int _adr = ret[4];
                            int _len = ret[5];
                            int _cnt = ret[6];
                            //  ショートパケットの長さは, 8+LEN バイト
                            bool long_packet = _id == 0;
                            packet_size = long_packet ? (8 + (1 + _len) * _cnt) : (8 + _len * _cnt);
                            if (ret.Length < packet_size) continue;
                            byte sum = ret[2];
                            for (int i = 3; i < packet_size - 1; i++) sum ^= ret[i];
                            if (ret[packet_size - 1] != sum) rexWrite("Checksum err.");
                            if (ret[0] == 0xFA)
                            {// 双葉モータへの送信データ
                                if (ret[1] != 0xAF) rexWrite("ERR");
                                if (long_packet) rexWrite("L(");
                                else
                                    rexWrite("S(");
                                rexWrite(packet_size + ") ");
                            }
                            else
                            {// 双葉モータからの返信
                                if (ret[1] != 0xDF) rexWrite("ERR");
                                rexWrite("R<");
                                rexWrite(string.Format("{0:X2}", ret[0]));
                                for (int i = 1; i < packet_size; i++)
                                {
                                    rexWrite(string.Format(" {0:X2}", ret[i]));
                                }
                                rexWrite("> " + Environment.NewLine);
                            }
                            //  1パケット読めたので, バッファーから削除
                            int copy_sz = (int)ret.Length - (packet_size);
                            rest = new byte[copy_sz];
                            Array.Copy(ret, packet_size, rest, 0, copy_sz);
                            ret = new byte[copy_sz];
                            rest.CopyTo(ret, 0);
                        }
                        else if (ret[0] == 0x04)
                        {//EOD
                            return;
                        }
                        else
                        {
                            rexWrite("TEXT<");
                            for (int i = 0; i < ret.Length; i++)
                            {
                                rexWrite(string.Format("{0:X2}", ret[i]) + " ");
                            }
                            rexWrite(">" + Environment.NewLine);
                            ret = new byte[0];
                            rest = new byte[0];
                        }
                        repeat = ret.Length > 0;
                    }
                }
                catch (InvalidCastException e)
                {
                    MessageBox.Show("えらーしたにゃ. しねよぼけ");
                }
                catch (TaskCanceledException e)
                {
                    MessageBox.Show("これって，いつおこるのかにゃあ");
                }
            }
        }
        private void send_SHORT(byte[] sendbuf)
        {
            //チェックサム計算
            int N = sendbuf.Length;
            byte sum = sendbuf[2];
            for (int i = 3; i < N - 1; i++) sum ^= sendbuf[i];
            sendbuf[N - 1] = sum;   //  CHECKSUM　ID以降DATA末尾までのXOR結果
            //送信
            logWrite("<");
            for (int i = 0; i < N; i++)
            {
                logWrite(string.Format("{0:X2}", sendbuf[i]) + " ");
            }
            logWrite(">");
            //送信デバイス
            string device = (string)SEND_DEV.Items[SEND_DEV.SelectedIndex];
            if (device == "USB RS-485 Port")
            {// REXで送信する
                logWriteln(".REX");
                //送信可能に設定
                REX.RtsEnable = true;
                //REX-USB70の稼働時間だけ待たないといけない.
                System.Threading.Thread.Sleep(1);
                REX.Write(sendbuf, 0, N);
                //ここも待たねばならんのだが・・・問題はRS405CBが0.1msで返事するところだな
                System.Threading.Thread.Sleep(1);
                REX.RtsEnable = false;
            }
            else
            {// FUTABAで送信する
                logWriteln(".RSC");
                FUTABA.Write(sendbuf, 0, N);
            }
            //int idx = Devices[device];
            //string filename = "COM" + Devices[device].ToString();
        }
        private void send_ACK_Click(object sender, EventArgs e)
        {
            int id = (int)servo_ID.Value;
            logWrite("ACK");
            byte[] sendbuf = new byte[8];
            sendbuf[0] = 0xFA;                  // HEADER
            sendbuf[1] = 0xAF;				    // HEADER
            sendbuf[2] = (byte)id;              // SERVO-ID
            sendbuf[3] = 0x01;  //  FLAGS    ACK-NACK要求
            sendbuf[4] = 0x00;  //  ADDRESS 書き込み先アドレス
            sendbuf[5] = 0x00;  //  LENGTH  書き込みバイト数
            sendbuf[6] = 0x00;  //  COUNT   書き込み対象となるサーボの数
            send_SHORT(sendbuf);
        }
        private void SEND_RESET_Click(object sender, EventArgs e)
        {
            int id = (int)servo_ID.Value;
            logWrite("RESET");
            byte[] sendbuf = new byte[8];
            sendbuf[0] = 0xFA;                 // HEADER
            sendbuf[1] = 0xAF;				   // HEADER
            sendbuf[2] = (byte)id;                 // SERVO-ID
            sendbuf[3] = 0x10;                  // FLAGS    ACK-NACK要求
            sendbuf[4] = 0xFF;  //  ADDRESS 書き込み先アドレス
            sendbuf[5] = 0xFF;  //  LENGTH  書き込みバイト数
            sendbuf[6] = 0x00;  //  COUNT   書き込み対象となるサーボの数
            send_SHORT(sendbuf);
        }
        private void SEND_ON_Click(object sender, EventArgs e)
        {
            Button me = (Button)sender;
            int id = (int)servo_ID.Value;
            //トルクをONする
            byte[] sendbuf = new byte[9];
            sendbuf[0] = 0xFA;                 // HEADER
            sendbuf[1] = 0xAF;				   // HEADER
            sendbuf[2] = (byte)id;             // SERVO-ID
            sendbuf[3] = 0x00;                 // FLAGS
            sendbuf[4] = 0x24;  //  ADDRESS 書き込み先アドレス
            sendbuf[5] = 0x01;  //  LENGTH  書き込みバイト数
            sendbuf[6] = 0x01;  //  COUNT   書き込み対象となるサーボの数
            if (me.Text == "ON")
            {
                logWrite("ON");
                sendbuf[7] = 0x01;  //  DATA    書き込みデータ
                me.Text = "OFF";
                SEND_MOVE.Enabled = true;
            }
            else
            {
                logWrite("OFF");
                sendbuf[7] = 0x00;  //  DATA    書き込みデータ
                me.Text = "ON";
                SEND_MOVE.Enabled = false;
            }
            send_SHORT(sendbuf);

        }
        private void SEND_MOVE_Click(object sender, EventArgs e)
        {
            int Pos = 10 * (int)move_angle.Value;
            int Time = (int)move_time.Value;
            int id = (int)servo_ID.Value;
            //サーボを移動する
            logWrite("MOVE");
            byte[] sendbuf = new byte[12];
            sendbuf[0] = 0xFA;                 // HEADER
            sendbuf[1] = 0xAF;				   // HEADER
            sendbuf[2] = (byte)id;             // SERVO-ID
            sendbuf[3] = 0x00;                 // FLAGS
            sendbuf[4] = 0x1E;  //  ADDRESS 書き込み先アドレス
            sendbuf[5] = 0x04;  //  LENGTH  書き込みバイト数
            sendbuf[6] = 0x01;  //  COUNT   書き込み対象となるサーボの数
            sendbuf[7] = (byte)(Pos & 0x00FF);            //  DATA    書き込みデータ
            sendbuf[8] = (byte)((Pos & 0xFF00) >> 8);     //  DATA    書き込みデータ
            sendbuf[9] = (byte)(Time & 0x00FF);         //  DATA    書き込みデータ
            sendbuf[10] = (byte)((Time & 0xFF00) >> 8); //  DATA    書き込みデータ
            send_SHORT(sendbuf);
        }
        private void GET_ANGLE_Click(object sender, EventArgs e)
        {
            int id = (int)servo_ID.Value;
            logWrite("STAT");
            byte[] sendbuf = new byte[8];
            sendbuf[0] = 0xFA;                 // HEADER
            sendbuf[1] = 0xAF;				   // HEADER
            sendbuf[2] = (byte)id;             // SERVO-ID
            sendbuf[3] = 0x09;                 // FLAG 0b1001: RETURN 0x2A-0x3B
            sendbuf[4] = 0x00;  //  ADDRESS 書き込み先アドレス
            sendbuf[5] = 0x00;  //  LENGTH  書き込みバイト数
            sendbuf[6] = 0x01;  //  COUNT   書き込み対象となるサーボの数
            send_SHORT(sendbuf);
        }

        private void QUERY_Click(object sender, EventArgs e)
        {
            int id = (int)servo_ID.Value;
            logWrite("INFO");
            byte[] sendbuf = new byte[8];
            sendbuf[0] = 0xFA;                 // HEADER
            sendbuf[1] = 0xAF;				   // HEADER
            sendbuf[2] = (byte)id;             // SERVO-ID
            sendbuf[3] = 0x03;                 // FLAG 0b0011: RETURN 0x00-0x1D
            sendbuf[4] = 0x00;  //  ADDRESS 書き込み先アドレス
            sendbuf[5] = 0x00;  //  LENGTH  書き込みバイト数
            sendbuf[6] = 0x01;  //  COUNT   書き込み対象となるサーボの数
            send_SHORT(sendbuf);

        }

        private void WRITE_ID_Click(object sender, EventArgs e)
        {
            bool flush = false;
            int id = (int)servo_ID.Value;
            if ((bool)new_ID.Tag)
            {
                logWrite("NEW-ID");
                byte[] sendbuf = new byte[9];
                sendbuf[0] = 0xFA;                 // HEADER
                sendbuf[1] = 0xAF;                 // HEADER
                sendbuf[2] = (byte)id;             // SERVO-ID
                sendbuf[3] = 0x00;                 // FLAGS
                sendbuf[4] = 0x04;  //  ADDRESS 書き込み先アドレス
                sendbuf[5] = 0x01;  //  LENGTH  書き込みバイト数
                sendbuf[6] = 0x01;  //  COUNT   書き込み対象となるサーボの数
                sendbuf[7] = (byte)(new_ID.Value);            //  DATA    書き込みデータ
                send_SHORT(sendbuf);
                new_ID.Tag = false;
                servo_ID.Value = new_ID.Value;
                id = (int)servo_ID.Value;
                flush = true;
            }
            if ((bool)new_BAUD.Tag)
            {
                logWrite("NEW-BAUD");
                byte[] sendbuf = new byte[9];
                sendbuf[0] = 0xFA;                 // HEADER
                sendbuf[1] = 0xAF;                 // HEADER
                sendbuf[2] = (byte)id;             // SERVO-ID
                sendbuf[3] = 0x00;                 // FLAGS
                sendbuf[4] = 0x06;  //  ADDRESS 書き込み先アドレス
                sendbuf[5] = 0x01;  //  LENGTH  書き込みバイト数
                sendbuf[6] = 0x01;  //  COUNT   書き込み対象となるサーボの数
                sendbuf[7] = (byte)(new_BAUD.SelectedIndex);            //  DATA    書き込みデータ
                send_SHORT(sendbuf);
                new_BAUD.Tag = false;
                BAUD.SelectedIndex = new_BAUD.SelectedIndex;
                flush = true;
            }
            if ((bool)Reply.Tag)
            {
                uint _ir = ((uint)Reply.Value - 100) / 50;
                logWrite("NEW-REPLY");
                byte[] sendbuf = new byte[9];
                sendbuf[0] = 0xFA;                 // HEADER
                sendbuf[1] = 0xAF;                 // HEADER
                sendbuf[2] = (byte)id;             // SERVO-ID
                sendbuf[3] = 0x00;                 // FLAGS
                sendbuf[4] = 0x07;  //  ADDRESS 書き込み先アドレス
                sendbuf[5] = 0x01;  //  LENGTH  書き込みバイト数
                sendbuf[6] = 0x01;  //  COUNT   書き込み対象となるサーボの数
                sendbuf[7] = (byte)(_ir);            //  DATA    書き込みデータ
                send_SHORT(sendbuf);
                Reply.Tag = false;
                flush = true;
            }
            if ((bool)L_limit.Tag)
            {
                int Pos = 10 * (int)L_limit.Value;
                logWrite("NEW-limit-L");
                byte[] sendbuf = new byte[10];
                sendbuf[0] = 0xFA;                 // HEADER
                sendbuf[1] = 0xAF;                 // HEADER
                sendbuf[2] = (byte)id;             // SERVO-ID
                sendbuf[3] = 0x00;                 // FLAGS
                sendbuf[4] = 0x0A;  //  ADDRESS 書き込み先アドレス
                sendbuf[5] = 0x02;  //  LENGTH  書き込みバイト数
                sendbuf[6] = 0x01;  //  COUNT   書き込み対象となるサーボの数
                sendbuf[7] = (byte)(Pos & 0x00FF);            //  DATA    書き込みデータ
                sendbuf[8] = (byte)((Pos & 0xFF00) >> 8);     //  DATA    書き込みデータ
                send_SHORT(sendbuf);
                L_limit.Tag = false;
                flush = true;
            }
            if ((bool)R_limit.Tag)
            {
                int Pos = 10 * (int)R_limit.Value;
                logWrite("NEW-limit-R");
                byte[] sendbuf = new byte[10];
                sendbuf[0] = 0xFA;                 // HEADER
                sendbuf[1] = 0xAF;                 // HEADER
                sendbuf[2] = (byte)id;             // SERVO-ID
                sendbuf[3] = 0x00;                 // FLAGS
                sendbuf[4] = 0x08;  //  ADDRESS 書き込み先アドレス
                sendbuf[5] = 0x02;  //  LENGTH  書き込みバイト数
                sendbuf[6] = 0x01;  //  COUNT   書き込み対象となるサーボの数
                sendbuf[7] = (byte)(Pos & 0x00FF);            //  DATA    書き込みデータ
                sendbuf[8] = (byte)((Pos & 0xFF00) >> 8);     //  DATA    書き込みデータ
                send_SHORT(sendbuf);
                R_limit.Tag = false;
                flush = true;
            }
            if (flush)
            {
                {
                    logWrite("WRITING");
                    byte[] sendbuf = new byte[8];
                    sendbuf[0] = 0xFA;                 // HEADER
                    sendbuf[1] = 0xAF;                 // HEADER
                    sendbuf[2] = (byte)id;             // SERVO-ID
                    sendbuf[3] = 0x40;                 // FLAGS WRITE FLASH
                    sendbuf[4] = 0xFF;  //  ADDRESS 書き込み先アドレス
                    sendbuf[5] = 0x00;  //  LENGTH  書き込みバイト数
                    sendbuf[6] = 0x00;  //  COUNT   書き込み対象となるサーボの数
                    send_SHORT(sendbuf);
                    MessageBox.Show("書き込み中(1秒)は, モーター電源を切らないで下さい．わかりましたか", "聞いてるかボケ", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                {
                    MessageBox.Show("では, PCを再起動します．いやちがった，モーターを再起動します.", "1秒待ったか?それは真実か?", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    logWrite("REBOOT");
                    byte[] sendbuf = new byte[8];
                    sendbuf[0] = 0xFA;                 // HEADER
                    sendbuf[1] = 0xAF;                 // HEADER
                    sendbuf[2] = (byte)id;             // SERVO-ID
                    sendbuf[3] = 0x20;                 // FLAGS WRITE FLASH
                    sendbuf[4] = 0xFF;  //  ADDRESS 書き込み先アドレス
                    sendbuf[5] = 0x00;  //  LENGTH  書き込みバイト数
                    sendbuf[6] = 0x00;  //  COUNT   書き込み対象となるサーボの数
                    send_SHORT(sendbuf);
                }
            }
            WRITE_ID.Enabled = false;
        }
        private void new_ID_ValueChanged(object sender, EventArgs e)
        {
            WRITE_ID.Enabled = true;
            new_ID.Tag = true;
        }
        private void new_BAUD_SelectedIndexChanged(object sender, EventArgs e)
        {
            WRITE_ID.Enabled = true;
            new_BAUD.Tag = true;
        }
        private void Reply_ValueChanged(object sender, EventArgs e)
        {
            WRITE_ID.Enabled = true;
            Reply.Tag = true;
        }
        private void L_limit_ValueChanged(object sender, EventArgs e)
        {
            WRITE_ID.Enabled = true;
            L_limit.Tag = true;
        }
        private void R_limit_ValueChanged(object sender, EventArgs e)
        {
            WRITE_ID.Enabled = true;
            R_limit.Tag = true;
        }
    }
}
